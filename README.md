# Scopyleft, Le Suivi

Ici on suit et on accompagne les prestataires en sous-traitance.

On leur permet de déposer leurs factures et l'ensemble de leurs autres documents (attestation URSSAF, extraits kbis…)
